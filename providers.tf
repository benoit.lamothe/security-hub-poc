terraform {
  required_version = "~> 1.1.6"

  backend "s3" {
    bucket   = "snbx-poc-terraform-ticksmith"
    key      = "snbx/security-hub"
    region   = "us-east-1"
    role_arn = "arn:aws:iam::987556989467:role/OrganizationAccountAccessRole"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.48"
    }
  }
}

# Provider configuration

locals {
  tenant_account_role = "arn:aws:iam::987556989467:role/OrganizationAccountAccessRole"
}

provider "aws" {
  assume_role {
    role_arn = local.tenant_account_role
  }
}
